package nessie;

public enum ReadCategory {
	EXONIC_PAIR (false), 
	INTERGENIC_PAIR (true), 
	INTERGENIC_GENEBODY_PAIR (true), 
	INTERGENIC_EXON_PAIR(true),
	GENEBODY_PAIR(true),
	UNMAPPED_PAIR(false),
	LOW_QUALITY_PAIR(false);
	
	final boolean passesFilter;
	ReadCategory(boolean passes)
	{
		this.passesFilter = passes;
	}
}

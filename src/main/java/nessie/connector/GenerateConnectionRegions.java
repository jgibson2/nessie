package nessie.connector;

import nessie.windower.Windowing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.cli.*;

import htsjdk.samtools.SamReader;
import nessie.FileTools;
import htsjdk.samtools.BamFileIoUtils;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMRecordIterator;


public class GenerateConnectionRegions {
	//read in regions bed file
	//get reads from filtered BAM file
	//get mates from filtered BAM file (or possibly original? Filtered for now)
	//run windowing algorithm on mates
	
	public static void main(String[] args) throws IOException
	{
		Options options = new Options();

		// required options
		options.addOption(new Option("a", "alignment", true, "Alignment file (SAM/BAM/CRAM)"));
		options.getOption("a").setRequired(true);
		options.addOption(new Option("b", "bed", true, "Input BED file with overexpressed regions"));
		options.getOption("b").setRequired(true);
		options.addOption(new Option("o", "output", true, "Output connection file name prefix"));
		options.getOption("o").setRequired(true);
		
		//optional options
		options.addOption(new Option("w", "window", true, "Window length"));
		options.addOption(new Option("c", "count", true, "Minimum count to be considered"));
		options.addOption(new Option("r", "ratio", true, "Ratio of window size to skip"));

		CommandLineParser parser = new DefaultParser();
		HelpFormatter formatter = new HelpFormatter();
		CommandLine cmd;

		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			formatter.printHelp("nessie", options);

			System.exit(1);
			return;
		}
		
		String BEDfilename = cmd.getOptionValue("b");
		String BAMfilename = cmd.getOptionValue("a");
		String ConnectionsFilename = cmd.getOptionValue("o") + ".cnct";
		
		int w = 500;
		double r = 1.0;
		int c = 0;
		
		if (cmd.hasOption("w")) {
			w = Integer.parseInt(cmd.getOptionValue("w"));
		}
		if (cmd.hasOption("r")) {
			r = Double.parseDouble(cmd.getOptionValue("r"));
		}
		if (cmd.hasOption("c")) {
			c = Integer.parseInt(cmd.getOptionValue("c"));
		}
		
		final int window = w;
		final double ratio = r;
		final int count = c;
		
		final SamReader inputFile = FileTools.openAlignmentFile(BAMfilename);
		final SamReader inputFile2 = FileTools.openAlignmentFile(BAMfilename);
		final BufferedReader reader = new BufferedReader(new FileReader(BEDfilename));
		final BufferedWriter writer = new BufferedWriter(new FileWriter(ConnectionsFilename));
		
		reader.lines().forEach((String line) -> {
			line = line.trim();
			final String[] data = line.split("\t");
			int lowerBound;
			int upperBound;
			if(data.length > 2)
			{
				String locus = data[0];
				lowerBound = Integer.parseInt(data[1]);
				upperBound = Integer.parseInt(data[2]);
				boolean hasStrand = true;
				boolean mutStrand = false; //false for positive, true for negative
				//check if the BED file has strand info
				try
				{
					mutStrand = data[5].equals("-");
				}
				catch(ArrayIndexOutOfBoundsException e)
				{
					hasStrand = false;
				}
				final boolean strand = mutStrand;
				
				SAMRecordIterator it = inputFile.queryContained(locus, lowerBound, upperBound); //get all reads in the region
				List<SAMRecord> mates = new LinkedList<SAMRecord>();
				final int[] regionCount = {0};
				
				if(hasStrand)
				{
					it.forEachRemaining((SAMRecord rec) -> {
						if(strand == rec.getReadNegativeStrandFlag()) //make sure they are on the same strand
						{
							SAMRecord mate = inputFile2.queryMate(rec);
							if(mate != null)
							{
								mates.add(mate); //get the mate
								regionCount[0]++;
							}
						}
					});
				}
				else
				{
					it.forEachRemaining((SAMRecord rec) -> {
						SAMRecord mate = inputFile2.queryMate(rec);
						if(mate != null)
						{
							mates.add(mate); //get the mate
							regionCount[0]++;
						}
					});
				}
				
				it.close();
				
				System.out.format("Region: %s : %d - %d, Region Count: %d\n", locus, lowerBound, upperBound, regionCount[0]);
				
				//combine mates into intervals
				if(mates.size() > 0)
				{
					mates.sort((SAMRecord a, SAMRecord b) -> {
						return a.getAlignmentStart() - b.getAlignmentStart(); //sort mates based on alignment start
					});
										
					int start = mates.get(0).getAlignmentStart();
					int end = mates.get(mates.size() - 1).getAlignmentEnd();
					
					/*
					for(SAMRecord mate : mates)
					{
						System.out.println(mate.getAlignmentStart());
					}
					*/
					
					System.out.println("Start: " + start + " End: " + end);
					ArrayList<Integer[]> ivals = new ArrayList<Integer[]>(Windowing.generateIntervals(window, start, end, ratio));
					
					//find # of reads in each interval
					//get the max interval
					//report the original interval + the new mate interval connection
					
					int curIval = 0;
					for(SAMRecord mate : mates)
					{
						//for each mate, add one to the count for the region.
						if(mate.getAlignmentStart() > ivals.get(curIval)[1])
						{
							//System.out.println(mate.getAlignmentStart());
							do
							{
								curIval++;
							} while (mate.getAlignmentStart() > ivals.get(curIval)[1]);
						}
						ivals.get(curIval)[2]++;
					}
					
					LinkedList<Integer[]> passing = new LinkedList<>();
					
					for(Integer[] ival : ivals)
					{
						if(ival[2] > 0)
						{
							passing.add(ival);
							System.out.print("Interval: " + Arrays.toString(ival) + " ");
						}
					}
					System.out.println();
					
					Integer[] maxIval = Windowing.maxCountInterval(Windowing.combineIntervals(passing)); //get the interval with the maximum count
					
					try {
						if(maxIval[2] > count)
						{
							//if the count is large enough, write to the output file
							writer.write(String.format("%s\t%d\t%d\t%d\t%d\t%d\n", locus, lowerBound, upperBound, maxIval[0], maxIval[1], maxIval[2]));
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
		writer.close();
		reader.close();
	}

}

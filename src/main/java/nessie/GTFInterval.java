package nessie;


public class GTFInterval {
	public final int start;
	public final int end;
	public final String strand;
	public final String feature;
	
	public GTFInterval(int start, int end, String feature, String strand)
	{
		this.start = start;
		this.end = end;
		this.feature = feature;
		this.strand = strand;
	}
	
	

}

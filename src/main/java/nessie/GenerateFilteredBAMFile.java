package nessie;

import nessie.ReadFilter;
import nessie.ReadCategory;
import nessie.FileTools;

import org.apache.commons.cli.*;

import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.BAMIndexer;
import htsjdk.samtools.BamFileIoUtils;
import htsjdk.samtools.SAMFileWriter;
import htsjdk.samtools.SAMFileWriterFactory;
import htsjdk.samtools.SAMRecord;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;


public class GenerateFilteredBAMFile {

	public static void main(String[] args) {
		HashSet<ReadCategory> passing = new HashSet<>();
		
		passing.add(ReadCategory.INTERGENIC_EXON_PAIR);
		//passing.add(ReadCategory.INTERGENIC_PAIR);
		
		// http://stackoverflow.com/questions/367706/how-to-parse-command-line-arguments-in-java
		Options options = new Options();

		// required options
		options.addOption(new Option("a", "alignment", true, "Alignment file (SAM/BAM/CRAM)"));
		options.getOption("a").setRequired(true);
		options.addOption(new Option("g", "gff", true, "GFF3/GTF annotation file"));
		options.getOption("g").setRequired(true);
		//options.addOption(new Option("l", "log", true, "Log file"));
		//options.getOption("l").setRequired(true);
		options.addOption(new Option("o", "output", true, "Output BAM file name prefix"));
		options.getOption("o").setRequired(true);

		// switches
		options.addOption(new Option("v", "verbose", false, "Verbose logging and read categorization"));

		// optional options
		options.addOption(new Option("q", "minmapq", true, "Minimum mapping quality"));

		CommandLineParser parser = new DefaultParser();
		HelpFormatter formatter = new HelpFormatter();
		CommandLine cmd;

		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			formatter.printHelp("nessie", options);

			System.exit(1);
			return;
		}

		String alignment = cmd.getOptionValue("a");
		String gtf = cmd.getOptionValue("g");
		String log = cmd.getOptionValue("l");
		String output = cmd.getOptionValue("o");
		boolean verbose = cmd.hasOption("v");
		int minmapq = 10;
		if (cmd.hasOption("q")) {
			minmapq = Integer.parseInt(cmd.getOptionValue("q"));
		}

		try {
			filterReads(alignment, gtf, log, output, verbose, minmapq, passing);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			System.exit(1);
			return;
		}

	}

	public static void filterReads(String alignment, String gtf, String log, String output, boolean verbose,
			int minmapq, HashSet<ReadCategory> passing) throws InterruptedException, IOException {
		final ReadFilter filter;
		try {
			System.out.println("Creating read filter object");
			filter = new ReadFilter(minmapq, "exon", "gene", gtf);
			System.out.println("Done creating read filter object");

		} catch (IOException e) {
			System.out.println(e.getMessage());
			System.exit(1);
			return;
		}
		System.out.println("Opening Alignment File");
		final SamReader inputFile = FileTools.openAlignmentFile(alignment);
		System.out.println("Done Opening Alignment File");

		if (verbose) {
			System.out.println("Opening output files");
			String exonicFilename = output + ".exonic.bam";
			String intergenicFilename = output + ".intergenic.bam";
			String intergenic_genebodyFilename = output + ".intergenic_genebody.bam";
			String intergenic_exonFilename = output + ".intergenic_exon.bam";
			String genebodyFilename = output + ".genebody.bam";

			final SAMFileWriter intermediate_exonic_output = new SAMFileWriterFactory()
					.makeBAMWriter(inputFile.getFileHeader(), false, new File(exonicFilename));
			final SAMFileWriter intermediate_intergenic_output = new SAMFileWriterFactory()
					.makeBAMWriter(inputFile.getFileHeader(), false, new File(intergenicFilename));
			final SAMFileWriter intermediate_intergenic_genebody_output = new SAMFileWriterFactory()
					.makeBAMWriter(inputFile.getFileHeader(), false, new File(intergenic_genebodyFilename));
			final SAMFileWriter intermediate_intergenic_exon_output = new SAMFileWriterFactory()
					.makeBAMWriter(inputFile.getFileHeader(), false, new File(intergenic_exonFilename));
			final SAMFileWriter intermediate_genebody_output = new SAMFileWriterFactory()
					.makeBAMWriter(inputFile.getFileHeader(), false, new File(genebodyFilename));
			
			final int[] exonicCount = {0}, intergenicCount = {0}, intergenic_genebodyCount = {0}, genebodyCount = {0}, intergenic_exonCount = {0};

			inputFile.forEach((SAMRecord rec) -> {
				ReadCategory cat = filter.filterRead(rec);
				//System.out.println("Here! " + cat);
				switch (cat) {
				case EXONIC_PAIR:
					intermediate_exonic_output.addAlignment(rec);
					exonicCount[0]++;
					break;
				case INTERGENIC_PAIR:
					intermediate_intergenic_output.addAlignment(rec);
					intergenicCount[0]++;
					break;
				case INTERGENIC_GENEBODY_PAIR: 
					intermediate_intergenic_genebody_output.addAlignment(rec);
					intergenic_genebodyCount[0]++;
					break;
				case INTERGENIC_EXON_PAIR: 
					intermediate_intergenic_exon_output.addAlignment(rec);
					intergenic_exonCount[0]++;
					break;
				case GENEBODY_PAIR:
					intermediate_genebody_output.addAlignment(rec);
					genebodyCount[0]++;
					break;
				default:
					break;
				}
			});

			intermediate_exonic_output.close();
			intermediate_intergenic_output.close();
			intermediate_intergenic_genebody_output.close();
			intermediate_genebody_output.close();
			System.out.println("Closed output files");
			try {
				inputFile.close();
				System.out.println("Closed input files");
			} catch (IOException e) {
				System.out.println(e.getMessage());
				System.exit(1);
				return;
			}
			
			System.out.println();
			System.out.println("----------- Total Reads Statistics-----------");
			System.out.format("Total Reads with Mates and Qualities above %d: %d\n", minmapq, exonicCount[0] + intergenic_genebodyCount[0] + intergenicCount[0] + genebodyCount[0] + intergenic_exonCount[0]);
			System.out.format("Total Reads Kept: %d\n", intergenic_exonCount[0] + intergenicCount[0]);
			System.out.println("----------- Read Type Statistics-----------");
			System.out.format("Intergenic + Exon Reads: %d\n", intergenic_exonCount[0]);
			System.out.format("Intergenic Reads: %d\n", intergenicCount[0]);
			System.out.format("Exonic Reads: %d\n", exonicCount[0]);
			System.out.format("Gene Body Reads: %d\n", genebodyCount[0]);
			System.out.format("Intergenic + Gene Body Reads: %d\n", intergenic_genebodyCount[0]);

			
		} else {
			
			final long[] filterTime = {0};
			
			System.out.println("Opening output file");
			final SAMFileWriter intermediate_output = new SAMFileWriterFactory()
					.makeBAMWriter(inputFile.getFileHeader(), false, new File(output + ".bam"));

			final int[] exonicCount = {0}, intergenicCount = {0}, intergenic_genebodyCount = {0}, genebodyCount = {0}, intergenic_exonCount = {0};
			inputFile.forEach((SAMRecord rec) -> {
				long time = System.nanoTime();
				ReadCategory cat = filter.filterRead(rec);
				filterTime[0] += System.nanoTime() - time;
				switch (cat) {
				case EXONIC_PAIR:
					exonicCount[0]++;
					break;
				case INTERGENIC_PAIR:
					intergenicCount[0]++;
					break;
				case INTERGENIC_GENEBODY_PAIR: 
					intergenic_genebodyCount[0]++;
					break;
				case INTERGENIC_EXON_PAIR: 
					intergenic_exonCount[0]++;
					break;
				case GENEBODY_PAIR:
					genebodyCount[0]++;
					break;
				default:
					break;
				}
				if(passing.contains(cat))
				{
					intermediate_output.addAlignment(rec);
				}
			});
			
			
			
			intermediate_output.close();
			try {
				inputFile.close();
				System.out.println("Closed input file");
			} catch (IOException e) {
				System.out.println(e.getMessage());
				System.exit(1);
				return;
			}
			System.out.println("Closed output file");
			
			System.out.format("Total time filtering (ms): %d\n", (long)(filterTime[0] / 1E6));
			
			System.out.println();
			System.out.println("----------- Total Reads Statistics-----------");
			System.out.format("Total Reads with Mates and Qualities above %d: %d\n", minmapq, exonicCount[0] + intergenic_genebodyCount[0] + intergenicCount[0] + genebodyCount[0] + intergenic_exonCount[0]);
			System.out.format("Total Reads Kept: %d\n", intergenic_exonCount[0] + intergenicCount[0]);
			System.out.println("----------- Read Type Statistics-----------");
			System.out.format("Intergenic + Exon Reads: %d\n", intergenic_exonCount[0]);
			System.out.format("Intergenic Reads: %d\n", intergenicCount[0]);
			System.out.format("Exonic Reads: %d\n", exonicCount[0]);
			System.out.format("Gene Body Reads: %d\n", genebodyCount[0]);
			System.out.format("Intergenic + Gene Body Reads: %d\n", intergenic_genebodyCount[0]);
		}

	}

}

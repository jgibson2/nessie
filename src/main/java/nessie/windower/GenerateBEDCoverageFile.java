package nessie.windower;

import nessie.FileTools;
import nessie.windower.Windowing;

import org.apache.commons.cli.*;

import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;
import htsjdk.samtools.BAMIndexer;
import htsjdk.samtools.BamFileIoUtils;
import htsjdk.samtools.SAMRecord;
import htsjdk.samtools.SAMRecordIterator;
import htsjdk.samtools.SAMSequenceRecord;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;


public class GenerateBEDCoverageFile {

	public static void main(String[] args) throws IOException {
		/* 
		 * Use SAMFileHeader getSequences(), call getSequenceLength() on each in 500BP windows
		 * Query using SAMReader query
		 * Count # of sequences and write to BED file
		 */
		
		Options options = new Options();

		// required options
		options.addOption(new Option("a", "alignment", true, "Alignment file (SAM/BAM/CRAM)"));
		options.getOption("a").setRequired(true);
		options.addOption(new Option("o", "output", true, "Output BED file name prefix"));
		options.getOption("o").setRequired(true);
		
		//optional options
		options.addOption(new Option("w", "window", true, "Window length"));
		options.addOption(new Option("c", "count", true, "Minimum count to be considered"));
		options.addOption(new Option("r", "ratio", true, "Ratio of window size to skip"));
		options.addOption(new Option("combine", false, "Combine likely intervals into larger intervals (results in inaccurate counts)"));

		CommandLineParser parser = new DefaultParser();
		HelpFormatter formatter = new HelpFormatter();
		CommandLine cmd;

		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			formatter.printHelp("nessie", options);

			System.exit(1);
			return;
		}
		
		String BEDfilename = cmd.getOptionValue("o") + ".bed";
		String BAMfilename = cmd.getOptionValue("a");
		int w = 500;
		double r = 1.0;
		int c = 0;
		boolean comb = false;
		if (cmd.hasOption("w")) {
			w = Integer.parseInt(cmd.getOptionValue("w"));
		}
		if (cmd.hasOption("r")) {
			r = Double.parseDouble(cmd.getOptionValue("r"));
		}
		if (cmd.hasOption("c")) {
			c = Integer.parseInt(cmd.getOptionValue("c"));
		}
		if (cmd.hasOption("combine")) {
			comb = true;
		}
		final int window = w;
		final double ratio = r;
		final int count = c;
		final boolean combine = comb;
		
		System.out.println("Opening alignment file");
		final SamReader inputFile = FileTools.openAlignmentFile(BAMfilename);
		final SamReader inputFile2 = FileTools.openAlignmentFile(BAMfilename);
		
		System.out.println("Opening output file");
		final BufferedWriter BEDWriter = new BufferedWriter(new FileWriter(new File(BEDfilename)));
		
		List<SAMSequenceRecord> sequences = inputFile2.getFileHeader().getSequenceDictionary().getSequences();
		inputFile2.close();
		System.out.println("Iterating over sequences and writing to BED file");
		for(SAMSequenceRecord rec : sequences) {
			final int seqLength = rec.getSequenceLength();
			final String seqName = rec.getSequenceName();
			List<Integer[]> intervals = Windowing.generateIntervals(window, seqLength, ratio);
			final LinkedList<Integer[]> passingNegIntervals = new LinkedList<>();
			final LinkedList<Integer[]> passingPosIntervals = new LinkedList<>();
			for(Integer[] interval : intervals) {
				final int[] fwdCount = {0};
				final int[] revCount = {0};
				
				//SAMRecordIterator iterator = inputFile.query(seqName, interval[0], interval[1], false);
				SAMRecordIterator iterator = inputFile.query(seqName, interval[0], interval[1], true);
				iterator.stream().sequential().forEach((SAMRecord seqRec) -> {
						if(seqRec.getReadNegativeStrandFlag())
						{
							revCount[0]++;
						}
						else
						{
							fwdCount[0]++;
						}
					}); //add one to count
				iterator.close();
				if(revCount[0] > count)
				{
					if(!combine) {
						BEDWriter.write(String.format("%s\t%d\t%d\t%s\t%d\t%s\n", seqName, interval[0], interval[1], ".", revCount[0], "-"));
					}
					else
					{
						Integer[] values = {interval[0], interval[1], revCount[0]};
						passingNegIntervals.add(values);
					}
				}
				if(fwdCount[0] > count)
				{
					if(!combine) {
						BEDWriter.write(String.format("%s\t%d\t%d\t%s\t%d\t%s\n", seqName, interval[0], interval[1], ".", fwdCount[0], "+"));
					}
					else
					{
						Integer[] values = {interval[0], interval[1], fwdCount[0]};
						passingPosIntervals.add(values);
					}
				}
			}
			if(combine)
			{
				//combine overlapping passing intervals
				for(Integer[] interval : Windowing.combineIntervals(passingNegIntervals))
				{
					BEDWriter.write(String.format("%s\t%d\t%d\t%s\t%d\t%s\n", seqName, interval[0], interval[1], ".", interval[2], "-")); // count inaccurate
				}
				for(Integer[] interval : Windowing.combineIntervals(passingPosIntervals))
				{
					BEDWriter.write(String.format("%s\t%d\t%d\t%s\t%d\t%s\n", seqName, interval[0], interval[1], ".", interval[2], "+")); //  count inaccurate
				}
			}
		}
		
		inputFile.close();
		BEDWriter.close();
		
		System.out.println("Done writing to BED file");

	}
	
	

}


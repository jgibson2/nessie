package nessie.windower;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

public final class Windowing {
	/*
	 * Generates intervals from 0 to limit of length window. Ratio determines how much the interval moves in between each step (from 0 to 1)
	 */
	public static List<Integer[]> generateIntervals(int window, int limit, double ratio)
	{
		List<Integer[]> intervals = new ArrayList<>();
		int current = 0;
		while(current < limit - window)
		{
			Integer[] interval = new Integer[2];
			interval[0] = current;
			interval[1] = current + window;
			//interval[2] = 0;
			intervals.add(interval);
			current += (int)Math.floor(window * ratio);
		}
		Integer[] interval = new Integer[3];
		interval[0] = current;
		interval[1] = limit;
		//interval[2] = 0;
		intervals.add(interval);
		
		return intervals;
	}
	
	public static LinkedList<Integer[]> generateIntervals(int window, int start, int end, double ratio)
	{
		LinkedList<Integer[]> intervals = new LinkedList<>();
		int current = start;
		while(current < end - window)
		{
			Integer[] interval = new Integer[3];
			interval[0] = current;
			interval[1] = current + window;
			interval[2] = 0;
			intervals.add(interval);
			current += (int)Math.floor(window * ratio);
		}
		Integer[] interval = new Integer[3];
		interval[0] = current;
		interval[1] = end;
		interval[2] = 0;
		intervals.add(interval);
		
		return intervals;
	}
	
	
	/*
	 * combines integers that have been put into a list 
	 */
	public static List<Integer[]> combineIntervals(LinkedList<Integer[]> intervals)
	{
		List<Integer[]> passing = new ArrayList<>();
		while(!intervals.isEmpty())
		{
			Integer[] interval = null;
			Integer[] next = null;
			try
			{
				interval = intervals.pop();
				next = intervals.pop();
				while(next[0] < interval[1]) // while the start of the next interval is less than the end of the current
				{
					interval[1] = next[1]; //combine the intervals
					interval[2] += next[2];
					next = intervals.pop(); //get the next interval
				}
			}
			catch (NoSuchElementException e)
			{
				
			}
			if(next != null)
			{
				intervals.push(next); //add skipped interval back onto list
			}
			Integer[] arr = {interval[0], interval[1], interval[2]};
			passing.add(arr); // count inaccurate
		}
		return passing;
	}
	
	public static Integer[] maxCountInterval(List<Integer[]> intervals)
	{
		Integer[] max = intervals.get(0);
		for(Integer[] interval : intervals)
		{
			if(interval[2] > max[2])
			{
				max = interval;
			}
		}
		return max;
	}
}

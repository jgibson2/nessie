package nessie;
import nessie.ReadCategory;
import nessie.GTFReader;
import java.io.IOException;
import java.util.List;

import htsjdk.samtools.SAMRecord;

public class ReadFilter{
	
	public final int minimumQuality;
	public final String exon_criteria, gene_criteria;
	//private final FeatureList features;
	private final GTFReader reader;
	
	public ReadFilter(int minimumQuality, String exon_criteria, String gene_criteria, String gtf_filename) throws IOException
	{
		this.minimumQuality = minimumQuality;
		this.exon_criteria = exon_criteria;
		this.gene_criteria = gene_criteria;
		System.out.println("Reading GTF");
		//this.features = GFF3Reader.read(gtf_filename);
		this.reader = new GTFReader(gtf_filename, exon_criteria, gene_criteria);
		System.out.println("Done Reading GTF");
	}
	
	public ReadCategory filterRead(SAMRecord read)
	{
		
		if(read.getMappingQuality() < this.minimumQuality)
		{
			return ReadCategory.LOW_QUALITY_PAIR;
		}
		if(read.getMateUnmappedFlag())
		{
			return ReadCategory.UNMAPPED_PAIR;
		}
		
		List<GTFInterval> readFeatures = null;
		List<GTFInterval> mateFeatures = null;
		try
		{
			readFeatures = this.reader.getOverlappingFeatures(read.getReferenceName(), read.getAlignmentStart(), read.getAlignmentEnd());
			mateFeatures = this.reader.getOverlappingFeatures(read.getMateReferenceName(), read.getMateAlignmentStart(), read.getMateAlignmentStart() + read.getReadLength());
			//inexact (does not factor in clipping or mate quality or splicing), but fast
		}
		catch(Exception e)
		{
			//e.printStackTrace();
			return ReadCategory.UNMAPPED_PAIR;
		}
		
		boolean readFeaturesInExon = false;
		boolean mateFeaturesInExon = false;
		boolean readFeaturesInGene = false;
		boolean mateFeaturesInGene = false;
		
		/*
		System.out.print("Read features: ");
		for(GTFInterval i : readFeatures)
		{
			System.out.print(i.feature + " ");
		}
		System.out.println();
		
		System.out.print("Mate features: ");
		for(GTFInterval i : mateFeatures)
		{
			System.out.print(i.feature + " ");
		}
		System.out.println();
		*/		
				
		for(GTFInterval i : readFeatures)
		{
			if(i.feature.equals(exon_criteria))
			{
				//System.out.println("Read has exon");
				readFeaturesInExon = true;
				break;
			}
		}
		for(GTFInterval i : readFeatures)
		{
			if(i.feature.equals(gene_criteria))
			{
				//System.out.println("Read has gene");
				readFeaturesInGene = true;
				break;
			}
		}
		for(GTFInterval i : mateFeatures)
		{
			if(i.feature.equals(exon_criteria))
			{
				//System.out.println("Mate has exon");
				mateFeaturesInExon = true;
				break;
			}
		}
		for(GTFInterval i : mateFeatures)
		{
			if(i.feature.equals(gene_criteria))
			{
				//System.out.println("Mate has gene");
				mateFeaturesInGene = true;
				break;
			}
		}
		
		if(readFeaturesInExon && mateFeaturesInExon)
		{
			return ReadCategory.EXONIC_PAIR;
		}
		else if((readFeaturesInExon && !mateFeaturesInGene) || (mateFeaturesInExon && !readFeaturesInGene))
		{
			return ReadCategory.INTERGENIC_EXON_PAIR;
		}
		else if(readFeaturesInGene || mateFeaturesInGene)
		{
			if(readFeaturesInGene && mateFeaturesInGene)
			{
				return ReadCategory.GENEBODY_PAIR;
			}
			else
			{
				return ReadCategory.INTERGENIC_GENEBODY_PAIR;
			}
		}
		else
		{
			return ReadCategory.INTERGENIC_PAIR;
		}
		
		
	}
	
}


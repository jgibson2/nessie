package nessie;

import htsjdk.samtools.util.IntervalTree;
import nessie.GTFInterval;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GTFReader {
	
	final HashMap<String, IntervalTree<GTFInterval>> itrees;
	final String[] criteria;
	
	public GTFReader(String filename, String... criteria) throws FileNotFoundException
	{
		itrees = new HashMap<String, IntervalTree<GTFInterval>>();
		this.criteria = criteria;
		parseGTF(filename);
	}
	
	private void parseGTF(String filename) throws FileNotFoundException
	{
		Scanner scanner = new Scanner(new File(filename));
		scanner.useDelimiter("\n");
		Pattern commentPattern = Pattern.compile("^#");
		scanner.forEachRemaining((String line) -> {
			line = line.trim();
			Matcher m = commentPattern.matcher(line);
			if(!m.matches())
			{
				//System.out.println("Reached record");
				String[] data = line.split("\t");
				/*
				for(String s : data)
				{
					System.out.print(s + " ");
				}
				System.out.println();
				*/
				if(data.length == 9)
				{
					if(!itrees.containsKey(data[0]))
					{
						itrees.put(data[0], new IntervalTree<GTFInterval>());
					}
					int start = Integer.parseInt(data[3]);
					int end = Integer.parseInt(data[4]);
					data[2] = data[2].trim();
					data[6] = data[6].trim();
					//if the type is in the criteria, store it
					for(String criterion : criteria)
					{
						if(data[2].equals(criterion))
						{
							itrees.get(data[0]).put(start, end, new GTFInterval(start, end, data[2], data[6]));
							//System.out.println("Put data in tree");
							break;
						}
					}
				}
			}
		});
		scanner.close();
	}
	
	public List<GTFInterval> getOverlappingFeatures(String ref, int start, int end)
	{
		List<GTFInterval> features = new LinkedList<>();
		itrees.get(ref).overlappers(start, end).forEachRemaining((IntervalTree.Node<GTFInterval> node)->{features.add(node.getValue());});
		return features;
	}
 
}

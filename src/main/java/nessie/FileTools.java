package nessie;

import java.io.File;

import htsjdk.samtools.BAMIndexer;
import htsjdk.samtools.BamFileIoUtils;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.SamReaderFactory;

public final class FileTools {
	public static SamReader openAlignmentFile(String filename) {
		File file = new File(filename);
		final SamReader reader = SamReaderFactory.makeDefault().open(file);
		if(BamFileIoUtils.isBamFile(file) && !reader.hasIndex()) {
		  BAMIndexer.createIndex(reader, new File(filename + ".bai"));
		}
		 
		return reader;
	}
}
